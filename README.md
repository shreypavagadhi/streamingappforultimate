# StreamingAppForUltimate

*** Ignore the targer.tar.gz and StreamingAppForUltimate.tar.gz as files are not uploaded properly because of network issue***

StreamingAppForUltimate is an online streaming flask app which process the tweets every 20s using spark streaming.
Each bach is then cleaned and removed unwanted characters like "#,"RT:" & URL. Once cleaned, each batch the latest cornavirus cases is fetched.

Later the accumulated data is stored in NoSQL database(MongoDB)

How to run the flask app

Step 1:
Download and unzip the StreamingAppForUltimateFinal.tar.gz

Step 2:
pip3 install -r requirements.txt to full fill all the dependencies first.

Step 3:
Run the below script to start the flask server
sh run-app.sh

Step 4:
Run the below curl command in the terminal
```
curl -X POST \
  http://127.0.0.1:5000/read_tweets\
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
  "spark":{
    "app_name": "StreamingAppForUltimate",
    "host": "localhost",
    "port": "5555"
	},
  "web_scrap": {
    "url": "https://www.worldometers.info/coronavirus/"
     },
  "db": {
    "formatDb": "com.mongodb.spark_backup.sql.DefaultSource",
    "uri": "mongodb://mongodb:27017/",
    "database": "stream_db",
    "collection": "tweet_covid_count",
    "mode":"append"
	}
}'
```


Step 5:
To view the MongoDB data
Connect to MongoDB shell interface
use stream_db;
db.tweet_covid_count.find();


